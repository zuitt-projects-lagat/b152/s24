// Users with 'y' in first/last name:
db.users.find(
    {
        $or:[
        	{"firstName": {$regex: 'y', $options: '$i'}},
        	{"lastName": {$regex: 'y', $options: '$i'}}
        ]
    },
    {
        "_id": 0,
        "email": 1,
        "isAdmin": 1
    }
)



// Admin Users with 'e' in first name:
db.users.find(
    {
        $and:[
        	{"firstName": {$regex: 'e', $options: '$i'}},
        	{"isAdmin": true}
        ]
    },
    {
        "_id": 0,
        "email": 1,
        "isAdmin": 1
    }
)

// Products with 'x' and price greater/equal to 50000:
db.products.find({
    $and:[
        {"name": {$regex: 'x', $options: '$i'}},
        {"price": {$gte: 50000}}
    ]
})



//Update products with price less than 2000:
db.products.updateMany(
    {
        "price": {$lt: 2000}
    },
    {
        $set: {"isActive": false}
    }
)



//Delete products with price greater than 20000:
db.products.deleteMany({"price": {$gt: 20000}})