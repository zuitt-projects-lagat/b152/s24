/*
	Mini-Activity

	Open a shell in our new query152 database.

	Insert 5 products in a new products collection with the following details:

	name - Iphone X
	price - 30000
	isActive - true

	name - Samsung Galaxy S21
	price - 51000
	isActive - true

	name - Razer Blackshark V2X
	price - 2800
	isActive - false

	name - RAKK Gaming Mouse
	price - 1800
	isActive - true

	name - Razer Mechanical Keyboard
	price - 4000
	isActive - true

*/

/*db.products.insertMany([

	{
		"name": "Iphone X",
		"price": 30000,
		"isActive": true
	},
	{
		"name": "Samsung Galaxy S21",
		"price": 51000,
		"isActive": true
	},
	{
		"name": "Razer Blackshark V2X",
		"price": 2800,
		"isActive": false
	},
	{
		"name": "RAKK Gaming Mouse",
		"price": 1800,
		"isActive": true
	},
	{
		"name": "Razer Mechanical Keyboard",
		"price": 4000,
		"isActive": true
	},

])*/


//Query Operators

    

    //Query operators allow for more flexible querying in MongoDB.

    //Instead of just being able to find/search for documents with exact values

    //We can use query operators define conditions instead of just specific criterias.

//$gt,$lt,$gte,$lte - numbers

    //$gt - query operator which means greater than.

        //db.products.find({"price":{$gt: 3000}})

        

    //$lt - query operator which means less than.

        //db.products.find({"price": {$lt:4000}})

        

    //$gte - query operator which means greater than or equal to

        //db.products.find({"price": {$gte: 30000}})

        

    //$lte - query operator which means less than or equal to

       //db.products.find({"price": {$lte:2800}})

       

    //Do you think we can use these query operators to expand the search criteria when updating?
        //Yes. and you can use query operators to expand the search criteria for 

        //updateOne,updateMany,deleteOne,deleteMany

        //db.products.updateMany({"price": {$gte:30000}},{$set:{"isActive":false}})


//Mini-Activity

/*
	Mini-Activity

	Insert 5 users in a new users collection with the following details:

	firstname - Mary Jane
	lastname - Watson
	email - mjtiger@gmail.com
	password - tigerjackpot15
	isAdmin - false

	firstname - Gwen
	lastname - Stacy
	email - stacyTech@gmail.com
	password - stacyTech1991
	isAdmin - true
	
	firstname - Peter
	lastname - Parker
	email - peterWebDev@gmail.com
	password - webdeveloperPeter
	isAdmin - true

	firstname - Jonah
	lastname - Jameson
	email - jjjameson@gmail.com
	password - spideyisamenace
	isAdmin - false

	firstname -Otto
	lastname - Octavius
	email - ottoOctopi@gmail.com
	password - docOck15
	isAdmin - true

*/


/*db.users.insertMany([

	{
		"firstname": "Mary Jane",
		"lastname": "Watson",
		"email": "mjtiger@gmail.com",
		"password": "tigerjackpot15",
		"isAdmin":false
	},
	{
		"firstname": "Gwen",
		"lastname": "Stacy",
		"email": "stacyTech@gmail.com",
		"password": "stacyTech1991",
		"isAdmin":true
	},
	{
		"firstname": "Peter",
		"lastname": "Parker",
		"email": "peterWebDev@gmail.com",
		"password": "webdeveloperPeter",
		"isAdmin":true
	},
	{
		"firstname": "Jonah",
		"lastname": "Jameson",
		"email": "jjjameson@gmail.com",
		"password": "spideyisamenace",
		"isAdmin":false
	},
	{
		"firstname": "Otto",
		"lastname": "Octavius",
		"email": "ottoOctopi@gmail.com",
		"password": "docOck15",
		"isAdmin":true
	}


])*/



//$regex - this query operator will allow us to find documents which 

//will match the characters/pattern of the characters we are looking for

        

        //db.users.find({"firstname":{$regex: 'O'}})

        //db.users.find({"lastname":{$regex:'o'}})
        

        //$options - used our regex will be case insensitive.

            //db.users.find({"lastname":{$regex:'o',$options: '$i'}})

        

        //You can also find for documents that matches a specific word

           //db.users.find({"email":{$regex:'web',$options: '$i'}})

           //db.products.find({name:{$regex: 'phone',$options: '$i'}})

           

        //Mini-Activity:

            //1. using $regex look for products which name has the word "razer" in it.

            //2. using $regex look for products which name has the word "rakk" in it.

            //db.products.find({name:{$regex: 'razer',$options: '$i'}})

            //db.products.find({name:{$regex: 'rakk',$options: '$i'}})

            

//$or and $and

            //$or operator - allows us to have a logical operation wherein we can look or find for documents 

            //which can satisfy at least one of our conditions.

            

            //db.products.find({$or:[{"name":{$regex: 'x', $options: '$i'}},{"price":{$lte:10000}}]})

            //db.products.find({$or:[{"name":{$regex: 'x', $options: '$i'}},{"price":30000}]})

            //db.users.find({$or:[{"firstname":{$regex: 'a',$options: '$i'}},{"isAdmin":true}]})

            //db.users.find({$or:[{"lastname":{$regex: 'w',$options: '$i'}},{"isAdmin":false}]})

            

            //$and operator - allows us to have a logical operation wherein we can look or find for documents

            //which can satisfy both of our conditions.

            //db.products.find({$and:[{"name":{$regex: 'razer',$options:'$i'}},{"price":{$gte:3000}}]})

            //db.products.find({$and:[{"name":{$regex: 'x', $options: '$i'}},{"price":30000}]})

            //db.users.find({$and:[{"lastname":{$regex: 'w',$options: '$i'}},{"isAdmin":false}]})

            //db.users.find({$and:[{"lastname": {$regex: 'a',$options: '$i'}},{"firstname":{$regex: 'e', $options: 'i'}}]})
//Field Projection

           //db.users.find({},{"_id":0,"password":0})

            //find() can have two arguments
            //db.collection.find({query},{projection})

            //Field Projection allows us to hide/show properties/fields of the 

            //returned documents after a query.
            //db.products.find({"price":{$lte:3000}},{"_id":0,"isActive":0})

            //In field projection, 0 means hide and 1 means show.

            //db.collection.find({query},{"field":0/1})
            //db.users.find({"isAdmin":false},{"_id":0,"email":1}

            //_id has to be explicitly hidden if you want to show only 1 field.
            //db.products.find({},{"_id":0,"price":1})

            db.products.find({"price":{$gte:10000}},{"_id":0,"name":1,"price":1})